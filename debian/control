Source: htseq
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Diane Trout <diane@ghic.org>,
           Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-debian,
               python3-setuptools,
               python3-all-dev,
               python3-numpy,
               python3-matplotlib,
               python3-pysam,
               swig,
               cython3
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/htseq
Vcs-Git: https://salsa.debian.org/med-team/htseq.git
Homepage: https://www-huber.embl.de/users/anders/HTSeq/doc/overview.html
Rules-Requires-Root: no

Package: python3-htseq
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: python-htseq
Description: Python3 high-throughput genome sequencing read analysis utilities
 HTSeq can be used to performing a number of common analysis tasks
 when working with high-throughput genome sequencing reads:
 .
   * Getting statistical summaries about the base-call quality scores to
     study the data quality.
   * Calculating a coverage vector and exporting it for visualization in
     a genome browser.
   * Reading in annotation data from a GFF file.
   * Assigning aligned reads from an RNA-Seq experiments to exons and
     genes.
